


$("#sub").click( function() {
    console.log('click');

    $.post( $("#myForm").attr("action"),
        $("#myForm :input").serializeArray(),
        function(info){ $("#result").html(info);
        console.log( $("#myForm :input").serializeArray() );
    });

    clearInput();
});

$("#myForm").submit( function() {
    return false;
});

function clearInput() {
    console.log('clear');
    $("#myForm :input").each( function() {
        $(this).val('');
    });
}
